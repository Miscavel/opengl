# OpenGL C++

An attempt to load .obj files and display them in OpenGL with Gouraud (Default) / Flat Shading.

Includes 1st person camera simulation to observe the objects in the world.

Control :
- WASD : Directions
- UHJK : Rotation
- P : Switch between Gouraud / Flat Shading

Note :
1. Project is built in Visual Studio 2010.
2. Some normals may be mapped in the wrong direction as they are calculated manually using cross-product.
3. For academic purposes, most of the algorithms are self-coded albeit not optimized.

Screenshots :
1. House

![](Screenshots/home.png)

2. Umbreon

![](Screenshots/umbreon.png)

3. Staircase

![](Screenshots/moon.png)

4. Groudon

![](Screenshots/groudon.png)

5. Birdview

![](Screenshots/birdview.png)

